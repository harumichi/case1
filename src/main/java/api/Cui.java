package api;

import radio.Cui2;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



public class Cui {

    public static void main(String[] args) {
        String path = "https://www.nhk.or.jp/r-news/podcast/nhkradionews.xml";

        parseXML(path);
    }

    public static void parseXML(String path) {
        try {
            DocumentBuilderFactory  factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder         builder = factory.newDocumentBuilder();
            Document                document = builder.parse(path);
            Element                 root = document.getDocumentElement();

            /* Get and print Title of RSS Feed. */
            NodeList                channel = root.getElementsByTagName("channel");
            NodeList                title = ((Element)channel.item(0)).getElementsByTagName("title");
            //System.out.println("\nTitle: " + title.item(0).getFirstChild().getNodeValue() + "\n");

            /* Get Node list of RSS items */
            NodeList                item_list = root.getElementsByTagName("item");
            //for (int i = 1; i <item_list.getLength(); i++) {
                Element  element = (Element)item_list.item(0);
                NodeList item_title = element.getElementsByTagName("title");
                NodeList item_mp3  = element.getElementsByTagName("enclosure");
               // System.out.println(" title: " + item_title.item(0).getFirstChild().getNodeValue());
                //System.out.println(item_mp3.item(0).getAttributes().getNamedItem("url").getNodeValue());
            // }
                String date = item_mp3.item(0).getAttributes().getNamedItem("url").getNodeValue();
                System.out.println(date);

        } catch (IOException e) {
            System.out.println("IO Exception");
        } catch (ParserConfigurationException e) {
            System.out.println("Parser Configuration Exception");
        } catch (SAXException e) {
            System.out.println("SAX Exception");
        }
        return; 
    }
    public static void player(String[] args) {
        String[] Command = { "cmd", "/c", "start"}; // 起動コマンドを指定する
        Runtime runtime = Runtime.getRuntime(); // ランタイムオブジェクトを取得する
            try {
                runtime.exec(Command); // 指定したコマンドを実行する
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
